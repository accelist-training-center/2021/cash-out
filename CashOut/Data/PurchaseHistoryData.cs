﻿using CashOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashOut.Data
{
    /// <summary>
    /// A static class for handling static purchase history data.
    /// </summary>
    public static class PurchaseHistoryData
    {
        /// <summary>
        /// A static object to store list of all purchase transactions that have made.
        /// </summary>
        public static List<PurchaseHistoryModel> Histories { get; set; } = new List<PurchaseHistoryModel>();

        public static decimal Total { get; set; }
    }
}
