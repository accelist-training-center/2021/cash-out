﻿using CashOut.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashOut.Data
{
    /// <summary>
    /// A static class for handling static user cart data.
    /// </summary>
    public static class CartData
    {
        /// <summary>
        /// A static object to store list of the added items in user cart.
        /// </summary>
        public static List<CartItemModel> CartItems { get; set; } = new List<CartItemModel>();

        /// <summary>
        /// Get the <seealso cref="CartItemModel"/> object from user cart.
        /// </summary>
        /// <param name="productCode">The requested Product Code.</param>
        /// <returns>Return the <seealso cref="CartItemModel"/> object.</returns>
        public static CartItemModel GetCartItem(string productCode)
        {
            foreach (var cartItem in CartData.CartItems)
            {
                if (productCode == cartItem.ProductCode)
                {
                    return cartItem;
                }
            }

            // Return null value if not found.
            return null;
        }
    }
}
