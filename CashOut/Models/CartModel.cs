﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashOut.Models
{
    /// <summary>
    /// A model class for displaying user cart data.
    /// A model class usually is in POCO format.
    /// Reference: https://en.wikipedia.org/wiki/Plain_old_CLR_object.
    /// </summary>
    public class CartModel
    {
        public string ProductCode { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }
    }
}
