﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashOut.Models
{
    /// <summary>
    /// A product model class for defining the structure of "product" object.
    /// </summary>
    public class ProductModel
    {
        /// <summary>
        /// Store the product code.
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// Store the product name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Store product stock.
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// Store the product price.
        /// </summary>
        public decimal Price { get; set; }
    }
}
