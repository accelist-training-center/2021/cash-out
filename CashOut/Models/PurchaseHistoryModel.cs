﻿namespace CashOut.Models
{
    /// <summary>
    /// A model class for storing the purchase history data.
    /// </summary>
    public class PurchaseHistoryModel
    {
        public string ProductCode { get; set; }

        public string ProductName { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal SubTotal { get; set; }
    }
}
