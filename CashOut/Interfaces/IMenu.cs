﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashOut.Interfaces
{
    /// <summary>
    /// Interface for defining Menu classes.
    /// </summary>
    public interface IMenu
    {
        void DisplayMenus();
    }
}
